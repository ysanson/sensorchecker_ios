//
//  DatabaseTests.swift
//  SensorCheckerTests
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import XCTest
@testable import SensorChecker
import GRDB

class DatabaseTests: XCTestCase {
    var dbQueue: DatabaseQueue!

    override func setUpWithError() throws {
        dbQueue = DatabaseQueue()
        try AppDatabase.migrator.migrate(dbQueue)
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try AppDatabase.migrator.migrate(dbQueue)
    }

    func testInsertedTourIsCorrect() throws {
        let date = Date.init()
        var tour = Tour(id: 1, date: date)
        try dbQueue.write {db in
            try tour.insert(db)
        }
        
        let retrievedTours = try dbQueue.read {db in
            return try Tour.fetchAll(db)
        }
        
        XCTAssert(retrievedTours.count == 1)
        XCTAssert(retrievedTours[0].id == 1)
    }
    
    func testWithMultipleTours_OrganizedByDate() throws {
        var date = Date.init()
        try dbQueue.write {db in
            for i in 0...5 {
                var tour = Tour(id: Int64(i), date: date)
                date = date.advanced(by: -60)
                try tour.insert(db)
            }
        }
        
        let retrievedTours = try dbQueue.read {db in
            return try Tour.orderByDate().fetchAll(db)
        }
        
        XCTAssert(retrievedTours.count == 6)
        XCTAssert(retrievedTours[0].date < retrievedTours[1].date)
        XCTAssert(retrievedTours[0].date < retrievedTours[5].date)
    }
    
    func testGetWithId_ReturnsCorrectObject() throws {
        let label2 = "Une autre action"
        var action1 = Action(id: 1, actionLabel: "Une action")
        var action2 = Action(id: 2, actionLabel: label2)
        try dbQueue.write {db in
            try action1.insert(db)
            try action2.insert(db)
        }
        XCTAssert(action2.id == 2)
        //let retrievedAction = try type(of: action2).CacheObject.insertInDB(object: action2)
        let retrievedAction = try Action.insertInDB(object: action2)
        XCTAssert(retrievedAction.actionLabel == label2)
    }
    
    func testInsertingMultipleDependenciesWorks() throws {
        var action = Action(id: 1, actionLabel: "Test Action")
        var state = SensorState(id: 1, stateCode: "TE", stateDesc: "Test")
        var type = MeasurementType(id: 1, measurement: "Tests", unit: "TE")
        var sensor = Sensor(id: 1, idMeasurementType: 1, reference: "Test", description: "Tests for sensor", supplier: "TEST", idState: 1, isModified: false)
        var tour = Tour(id: 1, date: Date.init())
        var tourSensor = TourSensor(id: 1, idTour: 1, idSensor: 1, sensorOrder: 1)
        var intervention = Intervention(id: nil, idAction: 1, idTourSensor: 1, comments: "Test intervention", idNewSensor: nil)
        try dbQueue.write {db in
            try action.insert(db)
            try state.insert(db)
            try type.insert(db)
            try sensor.insert(db)
            try tour.insert(db)
            try tourSensor.insert(db)
            try intervention.insert(db)
        }
        
        let interventions = try dbQueue.read {db in
            try Intervention.fetchAll(db)
        }
        
        XCTAssert(interventions.count == 1)
        XCTAssert(interventions[0].id == 1)
        struct InterventionInfo: FetchableRecord, Codable {
            var intervention: Intervention
            var action: Action
            var tourSensor: TourSensor
        }
        let interventionInfo: InterventionInfo? = try dbQueue.read {db in
            let request = Intervention.filter(key: 1).including(required: Intervention.action).including(required: Intervention.tourSensor)
            return try InterventionInfo.fetchOne(db, request)
        }
        
        XCTAssert(interventionInfo != nil)
        XCTAssert(interventionInfo?.action.id == 1)
        XCTAssert(interventionInfo?.tourSensor.id == 1)
        XCTAssert(interventionInfo?.intervention.id == 1)
    }

    /*
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
 */

}
