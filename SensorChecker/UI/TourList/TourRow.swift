//
//  TourRow.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

struct TourRow: View {
    @Environment(\.colorScheme) var colorScheme
    var tour: Tour
    @State var nbOfInterventions = 0
    @State var nbOfSensors = 0
    
    var tourState: some View {
        switch nbOfInterventions {
        case 0:
            return Text("⌚")
        case nbOfSensors:
            return Text("✅")
        default:
            return Text("🛠️")
        }
    }
    
    static let tourDateFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateStyle = .long
        formatter.locale = .current
        return formatter
    }()
    
    var body: some View {
        HStack {
            Text("Tour \(tour.id) due \(tour.date, formatter: TourRow.self.tourDateFormat)")
            Spacer()
            tourState
        }.padding()
            .onAppear() {
                if self.nbOfInterventions == 0 && self.nbOfSensors == 0 {
                    self.nbOfInterventions = Intervention.getNumberOfInterventions(forTour: self.tour.id)
                    self.nbOfSensors = TourSensor.getNumberOfSensors(forTour: self.tour.id)
                }
        }
    }
}

#if DEBUG
struct TourRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TourRow(tour: Tour(id: 1, date: Date.init()))
            TourRow(tour: Tour(id: 1, date: Date.init()), nbOfInterventions: 1)
            TourRow(tour: Tour(id: 1, date: Date.init()), nbOfInterventions: 1, nbOfSensors: 1)
            TourRow(tour: Tour(id: 1, date: Date.init()))
                .preferredColorScheme(.dark)
            TourRow(tour: Tour(id: 1, date: Date.init()), nbOfInterventions: 1)
                .preferredColorScheme(.dark)
            TourRow(tour: Tour(id: 1, date: Date.init()), nbOfInterventions: 1, nbOfSensors: 1)
                .preferredColorScheme(.dark)
        }
        .previewLayout(.fixed(width: 300, height: 70))
    }
}
#endif
