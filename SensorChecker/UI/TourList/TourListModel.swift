//
//  TourListModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import Then

/// The view model representation of the tour list view.
public class TourListModel: ObservableObject {
    @Published var isLoading = false
    @Published var showingProfile = false
    @Published var tours: [Tour] = []
    
    private var toursCancellable: AnyCancellable? = nil
    
    /// Logs out the user.
    ///
    /// - Returns: A boolean to indicate the actions were successful.
    /// The boolean is used to navigate to the login screen, but it's the UI role.
    func logout() -> Bool {
        UserDAL.logout()
        showingProfile.toggle()
        return true
    }
    
    /// Sends tthe stored changed to the webservice.
    /// Then, deletes the interventions, and sets the sensors back to normal.
    ///
    /// - Returns: A promise of completion
    func sendDataToServer() -> Promise<Bool> {
        return Promise {resolve, reject in
            SensorDAL.sendUpdatedSensors()
                .then(InterventionDAL.sendAllInterventions())
                .then {res in
                    if res {
                        Sensor.setSensorsToUnmodified()
                        Intervention.deleteInterventions()
                        resolve(true)
                    } else {
                        resolve(false)
                    }
            }.onError{error in reject(error)}
        }
    }
    
    /// Calls the web service to synchronize with the server.
    func refreshData() -> Bool {
        do {
            try AppDatabase.synchronizeWithServer()
            return true
        }
        catch let error {
            debugPrint(error)
            return false
        }
    }
    
    private func receiveCompletion(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            debugPrint(error.localizedDescription)
        }
    }
    
    //MARK: - Init for SwiftUI Previews
    
    init(tours: [Tour] = []) {
        if tours.count > 0 {
            self.tours = tours
        } else {
            self.tours = Tour.retrieveAll() ?? []
        }
    }

    //MARK: - Load and unload functions
    func load() -> Bool {
        if refreshData() {
            toursCancellable = Tour.retrievePublisher()
            .sink(receiveCompletion: self.receiveCompletion(completion:)) {t in self.tours = t}
            return true
        } else {
            return !logout()
        }
    }
    
    func unload() {
        toursCancellable?.cancel()
    }

}
