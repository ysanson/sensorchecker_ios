//
//  MainView.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI
import SwiftUIRefresh

struct TourList: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var toursViewModel = TourListModel()
    @ObservedObject var router: RootViewModel
    
    // The profile button to display
    var profileButton: some View {
        Button(action: { self.toursViewModel.showingProfile.toggle() }) {
            Image(systemName: "person.crop.circle")
                .imageScale(.large)
                .accessibility(label: Text("User Profile"))
                .padding()
        }
    }
    
    var body: some View {
        NavigationView {
            List {
                ForEach(self.toursViewModel.tours) {tour in
                    NavigationLink(destination: TourDetails(tour: tour)) {
                        TourRow(tour: tour)
                    }
                }
            }
            .pullToRefresh(isShowing: $toursViewModel.isLoading){
                _ = self.toursViewModel.refreshData()
                self.toursViewModel.isLoading.toggle()
            }
            .navigationBarTitle(Text("Tours"))
            .navigationBarItems(trailing: profileButton)
            
        }
        .sheet(isPresented: $toursViewModel.showingProfile) {
            Profile(router: self.router, showingProfile: self.$toursViewModel.showingProfile)
        }
        .onAppear() {
            if !self.toursViewModel.load() {
                self.router.currentPage = "login"
            }
        }
        .onDisappear() {
            self.toursViewModel.unload()
        }
    }
}

//MARK: - Preview

#if DEBUG
struct TourList_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TourList(toursViewModel: TourListModel(tours: [Tour(id: 1, date: Date.init()), Tour(id: 2, date: Date.init()), Tour(id: 3, date: Date.init())]), router: RootViewModel())
            
            TourList(toursViewModel: TourListModel(tours: [Tour(id: 1, date: Date.init()), Tour(id: 2, date: Date.init()), Tour(id: 3, date: Date.init())]), router: RootViewModel())
                .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
        
    }
}
#endif
