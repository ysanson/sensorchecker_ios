//
//  SensorViewModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 18/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine

final class SensorViewModel: ObservableObject {
    // This is initialized with
    @Published var sensorInfos: SensorInfos? = nil
    
    private var infosCancellable: AnyCancellable?
    
    private func receiveCompletion(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            debugPrint(error.localizedDescription)
        }
    }
    
    // MARK: - Init for SwiftUI previews
    
    init(sensorInfos: SensorInfos? = nil) {
        if sensorInfos != nil {
            self.sensorInfos = sensorInfos
        }
    }
    
    //MARK: - Load and unload functions
    
    func load(sensorId: Int64) {
        infosCancellable = Sensor.getSensorInfosPublisher(byId: sensorId).sink(receiveCompletion: self.receiveCompletion(completion:)) {infos in
            self.sensorInfos = infos
        }
    }
    
    func unload() {
        infosCancellable?.cancel()
    }
}
