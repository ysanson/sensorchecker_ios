//
//  SensorRow.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 18/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

struct SensorRow: View {
    var sensorId: Int64
    @ObservedObject var viewModel = SensorViewModel()
    
    var body: some View {
        HStack {
            Unwrap($viewModel.sensorInfos.wrappedValue) {infos in
                VStack(alignment: .leading) {
                    Text("Sensor \(infos.sensor.id) (\(infos.sensor.reference))")
                    Text("\(infos.measurementType.measurement) (\(infos.measurementType.unit))")
                    HStack (alignment: .center) {
                        Text("\(infos.sensorState.stateDesc)")
                        Spacer()
                        infos.sensor.isModified ?
                            Text("Modified").italic().foregroundColor(.green) : Text("Unmodified").italic().foregroundColor(.red)
                    }
                }.padding()
            }
            Spacer()
        }
        .frame(height: 80)
        .onAppear() {
            self.viewModel.load(sensorId: self.sensorId)
        }
        .onDisappear() {
            self.viewModel.unload()
        }
    }
}

#if DEBUG
struct SensorRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            SensorRow(sensorId: 1, viewModel: SensorViewModel(sensorInfos: SensorInfos(sensor: Sensor(id: 1, idMeasurementType: 1, reference: "STS30-DIS", description: "Description", supplier: "Supplier", idState: 1, isModified: true), measurementType: MeasurementType(id: 1, measurement: "Temp", unit: "°C"), sensorState: SensorState(id: 1, stateCode: "TB", stateDesc: "Bon état"))))
            
            SensorRow(sensorId: 1, viewModel: SensorViewModel(sensorInfos: SensorInfos(sensor: Sensor(id: 1, idMeasurementType: 1, reference: "STS30-DIS", description: "Description", supplier: "Supplier", idState: 1, isModified: false), measurementType: MeasurementType(id: 1, measurement: "Temp", unit: "°C"), sensorState: SensorState(id: 1, stateCode: "TB", stateDesc: "Bon état"))))
                .preferredColorScheme(.dark)
        }
        .previewLayout(.fixed(width: 300, height: 80))
    }
}
#endif
