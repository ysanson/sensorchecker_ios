//
//  TourDetails.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI
import CodeScanner
import SwipeCell

struct TourDetails: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel = TourDetailsViewModel()
    var tour: Tour
    
    // Configure the scanner button
    var scannerButton: some View {
        Button(action: {
            self.viewModel.isShowingScanner.toggle()
        }) {
            Image(systemName: "qrcode.viewfinder")
            .imageScale(.large)
            .accessibility(label: Text("Bring QRCode reader"))
            .padding()
        }
    }
    
    var body: some View {
        ZStack {
            List {
                ForEach(self.viewModel.sensors) {sensor in
                    NavigationLink(destination: InterventionView(sensor: sensor, tourId: self.tour.id)) {
                        SensorRow(sensorId: sensor.id)
                        .id(UUID())
                        .swipeLeft2Right(slots: [
                            Slot(image: {
                                Image(systemName: "hand.thumbsup.fill")
                            }, title: {
                                Text("Mark good")
                                .foregroundColor(.white)
                                .font(.footnote)
                                .fontWeight(.semibold)
                                .embedInAnyView()
                            }, action: {
                                self.viewModel.markDone(forSensor: sensor)
                            }, style: .init(background: .green, imageColor: .white, slotWidth: 100))
                        ])
                    }
                }
                .listRowInsets(EdgeInsets())
            }
            
            Unwrap(self.$viewModel.QRSensor.wrappedValue) { (sensor) in
                EmptyView()
                    .programmaticNavigation(to: InterventionView(sensor: self.viewModel.QRSensor!, tourId: self.tour.id), when: self.$viewModel.isNavigatingToQRSensor)
            }
        }
        .navigationBarTitle(Text("Details for tour \(tour.id)"), displayMode: .inline)
        .navigationBarItems(trailing: scannerButton)
        .onAppear() { self.viewModel.load(tour: self.tour) }
        .onDisappear() { self.viewModel.unload() }
        .sheet(isPresented: self.$viewModel.isShowingScanner) {
            CodeScannerView(codeTypes: [.qr], simulatedData: "1", completion: self.viewModel.handleScan)
        }
    }
}

#if DEBUG
struct TourDetails_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            TourDetails(viewModel: TourDetailsViewModel(sensors: [Sensor(id: 1, idMeasurementType: 1, reference: "STS30-DIS", description: "Description", supplier: "Supplier", idState: 1, isModified: false), Sensor(id: 1, idMeasurementType: 1, reference: "STS30-DIS", description: "Description", supplier: "Supplier", idState: 1, isModified: true)]), tour: Tour(id: 1, date: Date.init()))
            TourDetails(viewModel: TourDetailsViewModel(sensors: [Sensor(id: 1, idMeasurementType: 1, reference: "STS30-DIS", description: "Description", supplier: "Supplier", idState: 1, isModified: false), Sensor(id: 1, idMeasurementType: 1, reference: "STS30-DIS", description: "Description", supplier: "Supplier", idState: 1, isModified: true)]), tour: Tour(id: 1, date: Date.init()))
                .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
    }
}
#endif
