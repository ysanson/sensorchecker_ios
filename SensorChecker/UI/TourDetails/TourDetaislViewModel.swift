//
//  TourDetaislViewModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 18/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import CodeScanner

final class TourDetailsViewModel: ObservableObject {
    @Published var sensors: [Sensor] = []
    @Published var isShowingScanner = false
    @Published var isNavigatingToQRSensor = false
    var tour: Tour? = nil
    var QRSensor: Sensor? = nil
    
    
    private var sensorsCancellable: AnyCancellable? = nil
    
    private func receiveCompletion(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            debugPrint(error.localizedDescription)
        }
    }
    
    /// Handles the scan completion
    func handleScan(result: Result<String, CodeScannerView.ScanError>) {
        isShowingScanner = false
        switch result {
        case .success(let code):
            guard let sensorID = Int64(code) else {return}
            guard let sensor = sensors.first(where: {$0.id == sensorID}) else {return}
            
            QRSensor = sensor
            isNavigatingToQRSensor.toggle()
        case .failure(let error):
            debugPrint("Scan FAILED \(error)")
        }
    }
    
    /// Marks the sensor as passed and done.
    /// - Parameter sensor:the sensor to mark
    func markDone(forSensor sensor: Sensor) {
        guard tour != nil else { return }
        guard let tourSensorId: Int64 = TourSensor.getTourSensor(withSensorId: sensor.id, fromTour: tour!.id)?.id else {return}
        
        let intervention = Intervention(id: nil, idAction: 1, idTourSensor: tourSensorId, comments: "RAS", idNewSensor: nil)
        do {
            _ = Intervention.insertNewIntervention(newIntervention: intervention)
            try Sensor.updateState(forSensor: sensor.id, newState: sensor.idState)
        } catch let error {
            debugPrint(error)
        }
        
    }
    
    //MARK: - Init for SwiftUI previews
    
    init(sensors: [Sensor] = []) {
        if sensors.count > 0 {
            self.sensors = sensors
        }
    }
    
    //MARK: - Load and unload functions
    
    func load(tour: Tour) {
        self.tour = tour
        isNavigatingToQRSensor = false
        sensorsCancellable = Sensor.getSensorsPublisher(forTour: tour.id)
            .sink(receiveCompletion: self.receiveCompletion(completion:)) { tourSensors in self.sensors = tourSensors }
    }
    
    
    func unload() {
        sensorsCancellable?.cancel()
    }
    
}
