//
//  Profile.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 24/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

struct Profile: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel = ProfileViewModel()
    @ObservedObject var router: RootViewModel
    @Binding var showingProfile: Bool
    
    // This method is passed as a callback function to send data. Because it's huge.
    private func sendDataCallback() {
        self.viewModel.isLoading.toggle()
        self.viewModel.sendDataToServer()
            .then {res in
                if res {
                    self.showingProfile.toggle()
                }
        }.onError {error in debugPrint(error)}
            .finally {
                self.viewModel.isLoading.toggle()
        }
    }
    
    var body: some View {
        LoadingView(isShowing: self.$viewModel.isLoading) {
            VStack {
                Text("Your profile").font(.largeTitle)
                Unwrap(self.viewModel.user) {user in
                    Text("\(user.firstname) \(user.lastname)").font(.headline)
                    Text("Interventions:").font(.title)
                    List(self.viewModel.interventions) {intervention in
                        InterventionRow(interventionId: intervention.id)
                    }
                    Spacer()
                    Button(action: self.sendDataCallback) {
                        Text("Send data")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 300, height: 50)
                            .background(Color.blue)
                            .cornerRadius(15.0)
                    }
                    Button(action: {self.viewModel.showingAlert.toggle()}) {
                        Text("Logout")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 300, height: 50)
                            .background(Color.red)
                            .cornerRadius(15.0)
                    }
                    .alert(isPresented: self.$viewModel.showingAlert) {
                        Alert(title: Text("Confirm"),
                              message: Text("Logout ? You will lose everything."),
                              primaryButton: .cancel(),
                              secondaryButton: .destructive(Text("OK"), action: {
                                if self.viewModel.logout() {
                                    self.showingProfile.toggle()
                                    self.router.currentPage = "login"
                                }
                              }))
                    }
                }
            }
            .padding()
        }
        .onAppear() { self.viewModel.load()}
        .onDisappear() {self.viewModel.unload()}
    }
}

#if DEBUG
struct Profile_Previews: PreviewProvider {
    static var previews: some View {
        Profile(router: RootViewModel(), showingProfile: .constant(true))
    }
}
#endif
