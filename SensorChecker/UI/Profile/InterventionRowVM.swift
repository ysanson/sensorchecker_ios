//
//  InterventionRowVM.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 24/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import Then

final class InterventionRowVM: ObservableObject {
    @Published var interventionInfos: InterventionInfos? = nil
    
    private var infoCancellable: AnyCancellable?
    
    private func receiveCompletion(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            debugPrint(error.localizedDescription)
        }
    }
    
    //MARK: - Init for SwiftUI Previews
    
    init(intInfos: InterventionInfos? = nil) {
        if intInfos != nil {
            interventionInfos = intInfos
        }
    }
    
    // MARK: - Load and unload functions
    func load(interventionId: Int64?) {
        if interventionId != nil {
            infoCancellable = Intervention.getInterventionInfosPublisher(byId: interventionId!)
                .sink(receiveCompletion: self.receiveCompletion(completion:), receiveValue: {infos in
                    self.interventionInfos = infos
                })
        }
        
    }
    
    func unload() {
        infoCancellable?.cancel()
    }
}
