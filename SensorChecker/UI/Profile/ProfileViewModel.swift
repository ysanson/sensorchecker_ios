//
//  ProfileViewModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 24/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import Then

final class ProfileViewModel: ObservableObject {
    @Published var isLoading = false
    @Published var showingAlert = false
    @Published var interventions: [Intervention] = []
    var user: User? = nil
    
    private var interventionsCancellable: AnyCancellable? = nil
    
    /// Logs out the user.
    ///
    /// - Returns: A boolean to indicate the actions were successful.
    /// The boolean is used to navigate to the login screen, but it's the UI role.
    func logout() -> Bool {
        UserDAL.logout()
        return true
    }
    
    /// Sends tthe stored changed to the webservice.
    /// Then, deletes the interventions, and sets the sensors back to normal.
    ///
    /// - Returns: A promise of completion
    func sendDataToServer() -> Promise<Bool> {
        return Promise {resolve, reject in
            SensorDAL.sendUpdatedSensors()
                .then(InterventionDAL.sendAllInterventions())
                .then {res in
                    if res {
                        Sensor.setSensorsToUnmodified()
                        Intervention.deleteInterventions()
                        resolve(true)
                    } else {
                        resolve(false)
                    }
            }.onError{error in reject(error)}
        }
    }
    
    private func receiveCompletion(completion: Subscribers.Completion<Error>) {
        if case let .failure(error) = completion {
            debugPrint(error.localizedDescription)
        }
    }
    
    //MARK: - Load and unload functions
    func load() {
        interventionsCancellable = Intervention.retrievePublisher().sink(receiveCompletion: self.receiveCompletion(completion:)) {i in self.interventions = i}
        user = User.retrieveUser()
    }
    
    func unload() {
        interventionsCancellable?.cancel()
    }
}
