//
//  InterventionRow.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 24/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

struct InterventionRow: View {
    var interventionId: Int64?
    @ObservedObject var viewModel = InterventionRowVM()
    
    var body: some View {
        HStack {
            Unwrap($viewModel.interventionInfos.wrappedValue) {infos in
                VStack(alignment: .leading) {
                    Text("Intervention \(infos.intervention.id ?? 0) on sensor \(infos.tourSensor.idSensor)")
                    Text("Performed \(infos.action.actionLabel)")
                    Text("Comments: \(infos.intervention.comments ?? "")")
                }
            }.padding()
            Spacer()
        }
        .frame(height: 80)
        .onAppear() {
            self.viewModel.load(interventionId: self.interventionId)
        }
        .onDisappear() {
            self.viewModel.unload()
        }
    }
}

struct InterventionRow_Previews: PreviewProvider {
    static var previews: some View {
        Group {
             InterventionRow(interventionId: 1, viewModel: InterventionRowVM(intInfos: InterventionInfos(intervention: Intervention(id: 1, idAction: 1, idTourSensor: 1, comments: "RAS", idNewSensor: nil), action: Action(id: 1, actionLabel: "Rien"), tourSensor: TourSensor(id: 1, idTour: 1, idSensor: 1, sensorOrder: 1))))
            InterventionRow(interventionId: 1, viewModel: InterventionRowVM(intInfos: InterventionInfos(intervention: Intervention(id: 1, idAction: 1, idTourSensor: 1, comments: "RAS", idNewSensor: nil), action: Action(id: 1, actionLabel: "Rien"), tourSensor: TourSensor(id: 1, idTour: 1, idSensor: 1, sensorOrder: 1))))
                .preferredColorScheme(.dark)
        }
       .previewLayout(.fixed(width: 300, height: 80))
    }
}
