//
//  InterventionViewModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 19/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine

final class InterventionViewModel: ObservableObject {
    @Published public var actions: [Action] = Action.retrieveAll() ?? []
    @Published public var states: [SensorState] = SensorState.retrieveAll() ?? []
    @Published public var sensors: [Sensor] =  Sensor.retrieveAll() ?? []
    
    private var sensorId: Int64? = nil
    private var tourId: Int64? = nil
    
    @Published var interventionDescription: String = ""
    @Published var hasNewSensor: Bool = false
    @Published var newSensorId: String = ""
    @Published var selectedAction: Int = 0
    @Published var selectedState: Int = 0
    
    /// Saves the new intervention and updates the corresponding sensor in the database.
    ///
    /// - Returns:True if everything went well, false otherwise.
    func saveChanges() -> Bool {
        // We ensure that the sensorId and the tourId are set
        guard let sId = sensorId, let tId = tourId else { return false }
        // We ensure that the tourSensor exists and gets its ID.
        guard let tourSensorId: Int64 = TourSensor.getTourSensor(withSensorId: sId, fromTour: tId)?.id else {return false}
        
        let actionId = actions[selectedAction].id
        let stateId = states[selectedState].id
        // We ensure that the current sensor exists in the list we got.
        guard let currentSensor = sensors.first(where: { $0.id == sId }) else {return false}
        
        let idNewSensor: Int64? = hasNewSensor ? Int64(newSensorId) : nil
        let newIntervention = Intervention(id: nil, idAction: actionId, idTourSensor: tourSensorId, comments: interventionDescription, idNewSensor: idNewSensor)
        
        //We try to update the sensor and insert the intervention
        do {
            _ = Intervention.insertNewIntervention(newIntervention: newIntervention)
            try Sensor.updateState(forSensor: currentSensor.id, newState: stateId)
            return true
        } catch let error {
            debugPrint(error)
            return false
        }
    }

    //MARK: - Load and unload functions
    
    func load(tourId: Int64, sensorId: Int64) {
        self.tourId = tourId
        self.sensorId = sensorId
        actions = Action.retrieveAll() ?? []
        states = SensorState.retrieveAll() ?? []
        sensors = Sensor.retrieveAll() ?? []
    }
}
