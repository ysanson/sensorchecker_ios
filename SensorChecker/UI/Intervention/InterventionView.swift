//
//  Intervention.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 18/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

struct InterventionView: View {
    var sensor: Sensor
    var tourId: Int64
    @ObservedObject var viewModel = InterventionViewModel()
    @Environment(\.presentationMode) var mode: Binding<PresentationMode>
    
    var doneButton: some View {
        Button(action: {
            if self.viewModel.saveChanges() {
                self.mode.wrappedValue.dismiss()
            }
        }) {
            Text("Done")
        }
    }
    
    var body: some View {
        Form {
            Section(header: Text("Sensor")) {
                Text("Sensor's current state")
                Picker(selection: self.$viewModel.selectedState, label: Text("State")) {
                    ForEach(0 ..< self.viewModel.states.count) {
                        Text("\(self.viewModel.states[$0].stateCode): \(self.viewModel.states[$0].stateDesc)").id(UUID())
                    }
                }
//                .pickerStyle(WheelPickerStyle())
            }
            Section(header: Text("Intervention")) {
                Text("Action taken")
                Picker(selection: self.$viewModel.selectedAction, label: Text("Action")) {
                    ForEach(0 ..< self.viewModel.actions.count) {
                        Text("\(self.viewModel.actions[$0].actionLabel)").id(UUID())
                    }
                }
                TextField("Comments", text: self.$viewModel.interventionDescription)
                    .multilineTextAlignment(.leading)
            }
            Section(header: Text("New sensor")) {
                Toggle(isOn: self.$viewModel.hasNewSensor) {
                    Text("Replaced the sensor")
                }
                if self.viewModel.hasNewSensor {
                    withAnimation {
                        TextField("ID of the new sensor", text: self.$viewModel.newSensorId)
                            .keyboardType(.numberPad)
                            .animation(.spring())
                            .id(UUID())
                        
                    }
                }
            }
        }
        .padding([.top])
        .navigationBarTitle("Intervention on \(sensor.reference)", displayMode: .inline)
        .navigationBarItems(trailing: doneButton)
        .onAppear() {
            self.viewModel.load(tourId: self.tourId, sensorId: self.sensor.id)
        }
    }
}

#if DEBUG
struct Intervention_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
            InterventionView(sensor: Sensor(id: 1, idMeasurementType: 1, reference: "Ref", description: "Desc", supplier: "Supplier", idState: 1, isModified: false), tourId: 1)
        }
    }
}
#endif
