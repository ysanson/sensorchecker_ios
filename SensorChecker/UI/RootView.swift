//
//  RootView.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 19/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

/// This view helps support a login page with the main page.
/// When a user is authenticated, the content changes to the main view. If there are no users, it is redirected to the login page.
struct RootView: View {
    @ObservedObject var router: RootViewModel
    
    var body: some View {
        ZStack {
            if self.router.currentPage == "login" {
                LoginView(router: router)
            } else {
                TourList(router: router)
            }
        }.onAppear() {
            self.router.load()
        }.onDisappear() {
            self.router.unload()
        }
    }
}

#if DEBUG
struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            RootView(router: RootViewModel())
            RootView(router: RootViewModel())
                .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
    }
}
#endif
