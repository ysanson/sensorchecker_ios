//
//  RootViewModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 19/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine

final class RootViewModel: ObservableObject {
    @Published var currentPage: String = "login"
    
    private var userCancellable: AnyCancellable? = nil
    
    private func receiveCompletion(completion: Subscribers.Completion<Error>) {
           if case let .failure(error) = completion {
               debugPrint(error.localizedDescription)
           }
       }
       
       //MARK: - Load and unload functions
       func load() {
           userCancellable = User.retrieveObservableUser().receive(on: RunLoop.main).eraseToAnyPublisher()
               .sink(receiveCompletion: self.receiveCompletion(completion:)) {user in
               if user != nil {
                self.currentPage = "tourList"
               }
           }
       }
       
       func unload() {
           userCancellable?.cancel()
       }
    
}
