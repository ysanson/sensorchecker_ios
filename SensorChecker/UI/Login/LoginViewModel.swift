//
//  LoginViewModel.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 17/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import Then

final class LoginViewModel: ObservableObject {
    @Published var username: String = "admin"
    @Published var password: String = "admin"
    @Published var isLoading: Bool = false
    @Published private var isAuthComplete = false
    
    func authenticate() -> Promise<Bool> {
        if !username.isEmpty && !password.isEmpty {
            return UserDAL.authenticateUser(withUsername: self.username, withPassword: self.password)
                .noMatterWhat {
                    self.isLoading = false 
            }
        } else {
            return Promise.resolve(false)
        }
    }
}

