//
//  ContentView.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI
import Then

struct LoginView: View {
    @Environment(\.colorScheme) var colorScheme
    @ObservedObject var viewModel = LoginViewModel()
    @ObservedObject var router: RootViewModel
    
    //Passed for the sign in callback button method, because it's cleaner.
    private func singInCallback() {
        self.viewModel.isLoading = true
        self.viewModel.authenticate()
            .then {isAuth in
                if isAuth {
                    self.router.currentPage = "tourList"
                }
        }
    }
    
    var body: some View {
        LoadingView(isShowing: self.$viewModel.isLoading) {
            VStack{
                Text("Sensor Checker iOS")
                    .font(.largeTitle).foregroundColor(.accentColor)
                    .padding([.top, .bottom], 40)
                
                VStack (alignment: .center, spacing: 20) {
                    TextField("Username", text: self.$viewModel.username)
                        .autocapitalization(.none)
                        .padding()
                        .background(Color.themeTextField)
                        .foregroundColor(.black)
                        .cornerRadius(20.0)
                    SecureField("Password", text: self.$viewModel.password)
                        .padding()
                        .background(Color.themeTextField)
                        .foregroundColor(.black)
                        .cornerRadius(20.0)
                    Button(action: self.singInCallback){
                        Text("Sign In")
                            .font(.headline)
                            .foregroundColor(.white)
                            .padding()
                            .frame(width: 300, height: 50)
                            .background(Color.green)
                            .cornerRadius(15.0)
                    }
                }.padding([.leading, .trailing], 27.5)
                Spacer()
                Image("logo-viveris")
                Spacer()
            }.background(self.colorScheme == .dark ? (Color(UIColor(hex: "#36393FFF") ?? .black)) : Color(UIColor(hex: "F3EBE4FF") ?? .white))
        }
    }
}

extension Color {
    static var themeTextField: Color {
        return Color(red: 220.0/255.0, green: 230.0/255.0, blue: 230.0/255.0, opacity: 1.0)
    }
}

#if DEBUG
struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        Group {
            LoginView(router: RootViewModel())
            LoginView(router: RootViewModel())
                .preferredColorScheme(.dark)
        }
        .previewLayout(.sizeThatFits)
    }
}
#endif
