//
//  ProgrammaticNav.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 25/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import SwiftUI

extension View {
    
    func programmaticNavigation<SomeView: View>(to view: SomeView, when binding: Binding<Bool>) -> some View {
        return modifier(ProgrammaticNavModifier(destination: view, binding: binding))
    }
}


fileprivate struct ProgrammaticNavModifier<SomeView: View>: ViewModifier {
    
    let destination: SomeView
    @Binding fileprivate var binding: Bool
    
    fileprivate func body(content: Content) -> some View {
        ZStack {
            content
            NavigationLink(destination: destination, isActive: $binding) {
                EmptyView()
            }
        }
    }
}
