//
//  TourSensor.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference

struct TourSensor: Hashable, Identifiable {
    var id: Int64
    var idTour: Int64
    var idSensor: Int64
    var sensorOrder: Int8
}

// MARK: - Persistence

extension TourSensor: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let idTour = Column(CodingKeys.idTour)
        static let idSensor = Column(CodingKeys.idSensor)
        static let sensorOrder = Column(CodingKeys.sensorOrder)
    }
    
    //Foreign keys definitions
    static let tourForeignKey = ForeignKey([Columns.idTour])
    static let sensorForeignKey = ForeignKey([Columns.idSensor])
    
    static let tour = belongsTo(Tour.self, using: tourForeignKey)
    static let sensor = belongsTo(Sensor.self, using: sensorForeignKey)
    
    static let inInterventions = hasMany(Intervention.self, using: Intervention.tourSensorForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}


// MARK: - DAO

extension TourSensor: DAOCache {
    
    //MARK: - DAOCache implementation
    static func synchronizeTableWithServer(withNewData newData: [TourSensor], extistingData: [TourSensor]) throws {
        //If there are no elements in the DB, we just insert all the new ones
        guard extistingData.count > 0 else {
            return try database.writeInTransaction {db in
                try newData.forEach {newObject in
                    var toInsert = newObject
                    try toInsert.insert(db)
                }
                return.commit
            }
        }
        
        return try database.writeInTransaction {db in
            for change in SortedDifference(left: extistingData, right: newData) {
                switch change {
                case .left(let dbObject):
                    try dbObject.delete(db) //Deleting the old objects
                case .right(var newObject):
                    try newObject.insert(db) //Inserting the new objects
                case .common(let dbObject, let newObject):
                    try newObject.updateChanges(db, from: dbObject) //Updating the common objects.
                }
            }
            return .commit
        }
    }
    
    static func retrievePublisher() -> DatabasePublishers.Value<[TourSensor]> {
        ValueObservation
        .tracking {db in try TourSensor.fetchAll(db)}
        .publisher(in: database)
    }
    
    static func retrieveAll() -> [TourSensor]? {
        do {
            return try database.read {db in
                try TourSensor.orderByPrimaryKey().fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    static func find(withId id: Int64?) -> TourSensor? {
        do {
            return try database.read {db in
                try TourSensor.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
    //MARK: - Custom DAO
    
    /// Retrieves the sensors for a specific tour.
    ///
    /// - Parameter tourId: The tour ID to find the sensors for.
    /// - Returns: The tour sensors for this tour.
    static func getSensors(forTour tourId: Int64?) -> [TourSensor]? {
        do {
            return try database.read {db in
                try TourSensor.filter(key: ["idTour": tourId]).fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    /// Finds and returns a tour sensor by the sensor id and the tour id provided.
    ///
    /// - Parameter sensorId: The sensor ID to find
    /// - Parameter tourId: the tour ID to find
    /// - Returns: The tour sensor, of nil if not found.
    static func getTourSensor(withSensorId sensorId: Int64, fromTour tourId: Int64) -> TourSensor? {
        do {
            return try database.read {db in
                try TourSensor.fetchOne(db, sql: "SELECT * FROM tourSensor WHERE idTour=:tourId AND idSensor=:sensorId", arguments: ["tourId": tourId, "sensorId": sensorId])
            }
        } catch {
            return nil
        }
    }
    
    /// Returns the number of sensors in a tour.
    ///
    /// - Parameter tourId: the tourID to check
    /// - Returns: The number of sensors for this tour, or 0 if error.
    static func getNumberOfSensors(forTour tourId: Int64) -> Int {
        do {
            return try database.read {db in
                try TourSensor.filter(sql: "idTour = ?", arguments: [tourId]).fetchCount(db)
            }
        } catch let error {
            debugPrint(error)
            return 0
        }
    }
}
