//
//  Intervention.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference


struct Intervention: Hashable, Identifiable {
    var id: Int64?
    var idAction: Int64
    var idTourSensor: Int64
    var comments: String?
    var idNewSensor: Int64?
}

// MARK: - Persistence

extension Intervention: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let idAction = Column(CodingKeys.idAction)
        static let idTourSensor = Column(CodingKeys.idTourSensor)
        static let comments = Column(CodingKeys.comments)
        static let idNewSensor = Column(CodingKeys.idNewSensor)
    }
    
    //Foreign keys definitions
    
    static let actionForeignKey = ForeignKey([Columns.idAction])
    static let tourSensorForeignKey = ForeignKey([Columns.idTourSensor])
    
    static let action = belongsTo(Action.self, using: actionForeignKey)
    static let tourSensor = belongsTo(TourSensor.self, using: tourSensorForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

/// Represents the association between an intervention, an action and the tour sensor.
struct InterventionInfos: FetchableRecord, Codable {
    var intervention: Intervention
    var action: Action
    var tourSensor: TourSensor
}

// MARK: - DAO

extension Intervention: DAOCache {
    /// This method just returns, since we don't receive data from the server, it just returns without doing anything.
    static func synchronizeTableWithServer(withNewData newData: [Intervention], extistingData: [Intervention]) throws {
        return
    }
    
    static func retrievePublisher() -> DatabasePublishers.Value<[Intervention]> {
        ValueObservation
            .tracking {db in try Intervention.fetchAll(db)}
            .publisher(in: database)
    }
    
    static func retrieveAll() -> [Intervention]? {
        do {
            return try database.read {db in
                try Intervention.orderByPrimaryKey().fetchAll(db)
            }
        } catch {
            return nil
        }
    }

    static func find(withId id: Int64?) -> Intervention? {
        do {
            return try database.read{db in
                try Intervention.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
    
    /// Deletes all interventions we have in the database.
    static func deleteInterventions() {
        do {
            return try database.write {db in
                try Intervention.deleteAll(db)
            }
        } catch let error {
            debugPrint(error)
        }
    }
    
    /// Gets the intervention by its tour sensor ID.
    /// Marked as private because it's no use for other parts of the code.
    ///
    /// - Parameter tourSensorID: The tourSensor to search an intervention for.
    ///
    /// - Returns: An intervention if there is one, or nil otherwise.
    private static func getIntervention(forTourSensor tourSensorID: Int64) -> Intervention? {
        do {
            return try database.read {db in
                return try Intervention.filter(sql: "idTourSensor = ?", arguments: [tourSensorID]).fetchOne(db)
            }
        } catch let error {
            debugPrint(error)
            return nil
        }
    }
    
    /// Inserts a new intervention for a tour sensor in the database.
    ///
    /// ##If an intervention already exists, it will be deleted.
    ///
    /// - Parameter int: The intervention to insert
    /// - Returns: The inserted intervention.
    static func insertNewIntervention(newIntervention int: Intervention) -> Intervention {
        var insertIntervention = int
        do {
            guard let existingIntervention = getIntervention(forTourSensor: int.idTourSensor) else {
                // If there are no already existing interventions, we just create it
                try database.write {db in
                    try insertIntervention.insert(db)
                }
                return insertIntervention
            }
            
            // Otherwise, we delete the previous, and insert the new intervention.
            
            try database.write {db in
                try existingIntervention.delete(db)
                try insertIntervention.insert(db)
            }
            
        } catch let error {
            debugPrint(error)
        }
        return insertIntervention
    }
    
    /// Gets the number of interventions for a specific tour.
    ///
    /// - Parameter tourId: The tour ID to search interventions for.
    /// - Returns: The number of interventions, 0 if none or error.
    static func getNumberOfInterventions(forTour tourId: Int64) -> Int {
        do {
            return try database.read {db in
                try Int.fetchOne(db, sql: "SELECT COUNT(i.id) FROM intervention i, tourSensor ts WHERE i.idTourSensor=ts.id AND ts.idTour=:tourId", arguments: ["tourId": tourId]) ?? 0
            }
        } catch let error {
            debugPrint(error)
            return 0
        }
    }
    
    /// Retrieves the infos for an intervention.
    /// - Parameter id: the intervention ID
    /// - Returns: A publisher containing the intervention infos, or null.
    static func getInterventionInfosPublisher(byId id: Int64) -> DatabasePublishers.Value<InterventionInfos?> {
        ValueObservation.tracking {db in
            let request = Intervention.filter(key: id).including(required: Intervention.action).including(required: Intervention.tourSensor)
            return try InterventionInfos.fetchOne(db, request)
        }.publisher(in: database)
    }
}
