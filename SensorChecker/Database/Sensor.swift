//
//  Sensor.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference

struct Sensor: Hashable, Identifiable {
    var id: Int64
    var idMeasurementType: Int64
    var reference: String
    var description: String
    var supplier: String
    var idState: Int64
    var isModified: Bool
}

// MARK: - Sensor Infos

/// Represents the association between a sensor, its measurement type, and its state.
/// More info on https://github.com/groue/GRDB.swift/blob/master/Documentation/AssociationsBasics.md
struct SensorInfos: FetchableRecord, Codable {
    var sensor: Sensor
    var measurementType: MeasurementType
    var sensorState: SensorState
}


// MARK: - Persistence

extension Sensor: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let idMeasurementType = Column(CodingKeys.idMeasurementType)
        static let reference = Column(CodingKeys.reference)
        static let description = Column(CodingKeys.description)
        static let supplier = Column(CodingKeys.supplier)
        static let idState = Column(CodingKeys.idState)
        static let isModified = Column(CodingKeys.isModified)
    }
    //Defining the foreign keys
    static let typeForeignKey = ForeignKey([Columns.idMeasurementType])
    static let stateForeignKey = ForeignKey([Columns.idState])
    
    static let type = belongsTo(MeasurementType.self, using: typeForeignKey)
    static let state = belongsTo(SensorState.self, using: stateForeignKey)
    
    //Defining the toursensor relationship
    static let createdTourSensors = hasMany(TourSensor.self, using: TourSensor.sensorForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// MARK: - DAO

extension Sensor: DAOCache {
    
    // MARK: - DAOCache implementation
    static func synchronizeTableWithServer(withNewData newData: [Sensor], extistingData: [Sensor]) throws {
        //If there are no elements in the DB, we just insert all the new ones
        guard extistingData.count > 0 else {
            return try database.writeInTransaction {db in
                try newData.forEach {newObject in
                    var toInsert = newObject
                    try toInsert.insert(db)
                }
                return.commit
            }
        }
        
        return try database.writeInTransaction {db in
            for change in SortedDifference(left: extistingData, right: newData) {
                switch change {
                case .left(let dbObject):
                    try dbObject.delete(db) //Deleting the old objects
                case .right(var newObject):
                    try newObject.insert(db) //Inserting the new objects
                case .common(let dbObject, let newObject):
                    try newObject.updateChanges(db, from: dbObject) //Updating the common objects.
                }
            }
            return .commit
        }
    }
    
    static func retrievePublisher() -> DatabasePublishers.Value<[Sensor]> {
        ValueObservation
            .tracking {db in try Sensor.fetchAll(db)}
            .publisher(in: database)
    }
    
    static func retrieveAll() -> [Sensor]? {
        do {
            return try database.read {db in
                try Sensor.orderByPrimaryKey().fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    static func find(withId id: Int64?) -> Sensor? {
        do {
            return try database.read {db in
                try Sensor.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
    
    // MARK: - Custom DAO for sensors
    
    /// Returns the sensors marked as modified in the database.
    ///
    /// - Returns: An array of modified sensors, or nil if an error occured.
    static func getModifiedSensors() -> [Sensor]? {
        do {
            return try database.read {db in
                try Sensor.fetchAll(db, sql: "select * from sensor where isModified = 1 order by id ASC")
            }
        } catch {
            return nil
        }
    }
    
    /// Returns tne unmodified sensors in the database.
    /// Same as the other function, but different.
    ///
    /// - Returns: An array of unmodified sensors, or nil if an error occured.
    static func getUnmodifiedSensors() -> [Sensor]? {
        do {
            return try database.read {db in
                try Sensor.fetchAll(db, sql: "select * from sensor where isModified = 0 order by id ASC")
            }
        } catch {
            return nil
        }
    }
    
    /// Gets the publisher containing the sensors for a specific tour.
    ///
    /// - Parameter tourId: The tour to retrieve sensors for.
    ///
    /// - Returns: The publisher containing the sensors.
    static func getSensorsPublisher(forTour tourId: Int64) -> DatabasePublishers.Value<[Sensor]> {
        let query = """
         SELECT s.id, s.idMeasurementType, s.reference, s.description, s.supplier, s.idState, s.isModified
         FROM sensor s, toursensor ts WHERE s.id=ts.idSensor and ts.idTour = :tourId ORDER BY ts.sensorOrder ASC
         """
        return ValueObservation
            .tracking {db in
                try Sensor.fetchAll(db, sql: query, arguments: ["tourId": tourId] )
        }
        .publisher(in: database)
    }
    
    /// Retrieves the different infos for a sensor, using the power of (associations)[https://github.com/groue/GRDB.swift/blob/master/Documentation/AssociationsBasics.md]
    /// Retrieves a publisher, to update in real time.
    ///
    /// - Parameter id: The sensor ID.
    ///
    /// - Returns: A DatabasePublisher containing a Sensorinfo object.
    static func getSensorInfosPublisher(byId id: Int64) -> DatabasePublishers.Value<SensorInfos?> {
        ValueObservation
            .tracking { db in
                let request = Sensor.filter(key: id).including(required: Sensor.type).including(required: Sensor.state)
                return try SensorInfos.fetchOne(db, request)
        }.publisher(in: database)
    }
    
    /// Updates a sensor in the database.
    ///
    /// - Parameter sensor: The sensor to update.
    static func updateSensor(updatedSensor sensor: Sensor) {
        do {
            try database.write {db in
                try sensor.update(db)
            }
        } catch let error {
            debugPrint(error)
        }
    }
    
    /// Updates only the state of a sensor, given its ID.
    ///
    /// - Parameter sensorId: the sensor to update
    /// - Parameter stateId: the new state of the sensor.
    ///
    /// - Throws: SQL errors
    static func updateState(forSensor sensorId: Int64, newState stateId: Int64) throws {
        try database.write {db in
            try db.execute(sql: "UPDATE sensor SET idState=:stateId, isModified=1 WHERE id=:sensorId", arguments: ["stateId": stateId, "sensorId": sensorId])
        }
    }
    
    /// Sets all sensors to an unmodified state.
    static func setSensorsToUnmodified() {
        do {
            try database.writeInTransaction {db in
                try db.execute(sql: "UPDATE sensor SET isModified=0 where isModified=1")
                return .commit
            }
        } catch let error {
            debugPrint(error)
        }
    }
}
