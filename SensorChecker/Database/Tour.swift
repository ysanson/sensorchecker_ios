//
//  Tour.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference

struct Tour: Hashable, Identifiable {
    var id: Int64
    var date: Date
}

// MARK: - Persistence

extension Tour: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let date = Column(CodingKeys.date)
    }
    
    static let hasTourSensors = hasMany(TourSensor.self, using: TourSensor.tourForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// MARK: - DAO

extension Tour: DAOCache {
    
    static func synchronizeTableWithServer(withNewData newData: [Tour], extistingData: [Tour]) throws {
        
        //If there are no elements in the DB, we just insert all the new ones
        guard extistingData.count > 0 else {
            return try database.writeInTransaction {db in
                try newData.forEach {newObject in
                    var toInsert = newObject
                    try toInsert.insert(db)
                }
                return.commit
            }
        }
        
        return try database.writeInTransaction {db in
            for change in SortedDifference(left: extistingData, right: newData) {
                switch change {
                case .left(let dbObject):
                    try dbObject.delete(db) //Deleting the old objects
                case .right(var newObject):
                    try newObject.insert(db) //Inserting the new objects
                case .common(let dbObject, let newObject):
                    try newObject.updateChanges(db, from: dbObject) //Updating the common objects.
                }
            }
            return .commit
        }
    }
    
    
    static func retrieveAll() -> [Tour]? {
        do {
            return try database.read {db in
                try Tour.orderByPrimaryKey().fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    static func find(withId id: Int64?) -> Tour? {
        do {
            return try database.read {db in
                try Tour.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
    
    static func retrievePublisher()-> DatabasePublishers.Value<[Tour]> {
        ValueObservation
            .tracking {db in try Tour.fetchAll(db)}
            .publisher(in: database)
    }
    
    /// Orders the tours by date.
    /// - Returns: A query interface request, used to then create SQL requests on it.
    static func orderByDate() -> QueryInterfaceRequest<Tour> {
        Tour.order(Columns.date)
    }
}
