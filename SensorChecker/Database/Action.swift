//
//  Action.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference

struct Action: Hashable, Identifiable {
    var id: Int64
    var actionLabel: String
}

// MARK: - Persistence

extension Action: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let actionLabel = Column(CodingKeys.actionLabel)
    }
    
    static let usedInInterventions = hasMany(Intervention.self, using: Intervention.actionForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// MARK: - DAO

extension Action: DAOCache {
    static func synchronizeTableWithServer(withNewData newData: [Action], extistingData: [Action]) throws {
        //If there are no elements in the DB, we just insert all the new ones
        guard extistingData.count > 0 else {
            return try database.writeInTransaction {db in
                try newData.forEach {newObject in
                    var toInsert = newObject
                    try toInsert.insert(db)
                }
                return.commit
            }
        }

        return try database.writeInTransaction {db in
            for change in SortedDifference(left: extistingData, right: newData) {
                switch change {
                case .left(let dbObject):
                    try dbObject.delete(db) //Deleting the old objects
                case .right(var newObject):
                    try newObject.insert(db) //Inserting the new objects
                case .common(let dbObject, let newObject):
                    try newObject.updateChanges(db, from: dbObject) //Updating the common objects.
                }
            }
            return .commit
        }
    }
    
    static func retrievePublisher() -> DatabasePublishers.Value<[Action]> {
        ValueObservation
        .tracking {db in try Action.fetchAll(db)}
        .publisher(in: database)
    }
    
    static func retrieveAll() -> [Action]? {
        do {
            return try database.read {db in
                try Action.orderByPrimaryKey().fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    static func find(withId id: Int64?) -> Action? {
        do {
            return try database.read {db in
                try Action.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
}
