//
//  DAOProtocol.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 13/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import GRDB
import GRDBCombine
import SortedDifference

/// #This protocol provides some generic functions that each cache object must implement.
/// This does not cover specific use cases (for example, if we want to execute a particular query), but it provides a good starting point.
protocol DAOCache {
    /// The generic type that encapsulates what the database objects conform to.
    associatedtype CacheObject: Identifiable, Hashable, Codable, FetchableRecord, MutablePersistableRecord
    
    /// Retrieves all lines from a particular table
    ///
    /// - Returns: An array containing all the lines of that table. It can be nil, if the execution encountered an error.
    static func retrieveAll() -> [CacheObject]?
    
    /// Retrieves all the objects in the DB, and provides an interface to receive updates whenever the objects changes.
    ///
    /// - Returns: A Database Publisher containing the array of objects, from which we can subscribe to receive updates.
    static func retrievePublisher() -> DatabasePublishers.Value<[CacheObject]>
    
    /// Finds a particular object, by its ID in the database.
    ///
    /// - Returns: this object, or nil if not found/an arror occured.
    static func find(withId id: Int64?) -> CacheObject?
    
    /// Synchronizes with external changes.
    /// Deletes the old elements, adds the new ones, updates the existing.
    /// The elements must be sorted beforehand.
    ///
    /// - Parameter existingData: the actual objects in the database
    /// - Parameter newData: the new objects received.
    ///
    /// - Throws: SQL errors, most likely.
    static func synchronizeTableWithServer(withNewData newData: [CacheObject], extistingData: [CacheObject]) throws
}

/// This extension serves to provide a way to implement static methods for the DAOCache protocol.
/// It's because they are the same for every CacheObject.
/// They are called in a hacky way, but it works ™. Here is an example for `insertInDB`.
/// ```Swift
///     let tour = Tour(...)
///     let insertedTour = try! type(of: tour).CacheObject.insertInDB(tour)
/// ```
/// Alternatively, you can invoke ir simply by calling it from the structure:
///```Swift
///     let tour = Tour(...)
///     let insertedTour = Tour.insertInDB(object: tour)
/// ```
///It's simpler! 
extension DAOCache {
    /// Inserts the CacheObjet in the database. Since the cache object is a table object, it just works ™
    ///
    /// - Parameter object: the CacheObject to insert. It **must** conform to a table existing in the database.
    /// - Throws: A SQLite error, most probably
    /// - Returns: The updated object (mainly the new object's ID)
    static func insertInDB(object: CacheObject) throws -> CacheObject {
        var insertedObject = object
        try database.write {db in
            try insertedObject.insert(db)
        }
        return object
    }
}
