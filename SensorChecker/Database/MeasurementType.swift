//
//  MeasurementType.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference

struct MeasurementType: Hashable, Identifiable {
    var id: Int64
    var measurement: String
    var unit: String
}

// MARK: - Persistence

extension MeasurementType: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let measurement = Column(CodingKeys.measurement)
        static let unit = Column(CodingKeys.unit)
    }
    
    static let sensors = hasMany(Sensor.self, using: Sensor.typeForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// MARK: - DAO

extension MeasurementType: DAOCache {
    static func synchronizeTableWithServer(withNewData newData: [MeasurementType], extistingData: [MeasurementType]) throws {
        //If there are no elements in the DB, we just insert all the new ones
        guard extistingData.count > 0 else {
            return try database.writeInTransaction {db in
                try newData.forEach {newObject in
                    var toInsert = newObject
                    try toInsert.insert(db)
                }
                return.commit
            }
        }

        return try database.writeInTransaction {db in
            for change in SortedDifference(left: extistingData, right: newData) {
                switch change {
                case .left(let dbObject):
                    try dbObject.delete(db) //Deleting the old objects
                case .right(var newObject):
                    try newObject.insert(db) //Inserting the new objects
                case .common(let dbObject, let newObject):
                    try newObject.updateChanges(db, from: dbObject) //Updating the common objects.
                }
            }
            return .commit
        }
    }
    
    static func retrievePublisher() -> DatabasePublishers.Value<[MeasurementType]> {
        return ValueObservation
            .tracking {db in try MeasurementType.fetchAll(db)}
            .publisher(in: database)
    }
    
    static func retrieveAll() -> [MeasurementType]? {
        do {
            return try database.read {db in
                try MeasurementType.orderByPrimaryKey().fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    static func find(withId id: Int64?) -> MeasurementType? {
        do {
            return try database.read {db in
                try MeasurementType.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
}
