//
//  SensorState.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import SortedDifference

struct SensorState: Hashable, Identifiable {
    var id: Int64
    var stateCode: String
    var stateDesc: String
}


// MARK: - Persistence

extension SensorState: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns {
        static let id = Column(CodingKeys.id)
        static let stateCode = Column(CodingKeys.stateCode)
        static let stateDesc = Column(CodingKeys.stateDesc)
    }
    
    static let sensorsWithState = hasMany(Sensor.self, using: Sensor.stateForeignKey)
    
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// MARK: - DAO

extension SensorState: DAOCache {
    static func synchronizeTableWithServer(withNewData newData: [SensorState], extistingData: [SensorState]) throws {
        //If there are no elements in the DB, we just insert all the new ones
        guard extistingData.count > 0 else {
            return try database.writeInTransaction {db in
                try newData.forEach {newObject in
                    var toInsert = newObject
                    try toInsert.insert(db)
                }
                return.commit
            }
        }

        return try database.writeInTransaction {db in
            for change in SortedDifference(left: extistingData, right: newData) {
                switch change {
                case .left(let dbObject):
                    try dbObject.delete(db) //Deleting the old objects
                case .right(var newObject):
                    try newObject.insert(db) //Inserting the new objects
                case .common(let dbObject, let newObject):
                    try newObject.updateChanges(db, from: dbObject) //Updating the common objects.
                }
            }
            return .commit
        }
    }
    
    static func retrievePublisher() -> DatabasePublishers.Value<[SensorState]> {
        ValueObservation
        .tracking {db in try SensorState.fetchAll(db)}
        .publisher(in: database)
    }
    
    static func retrieveAll() -> [SensorState]? {
        do {
            return try database.read {db in
                try SensorState.fetchAll(db)
            }
        } catch {
            return nil
        }
    }
    
    static func find(withId id: Int64?) -> SensorState? {
        do {
            return try database.read {db in
                try SensorState.fetchOne(db, key: id)
            }
        } catch {
            return nil
        }
    }
}
