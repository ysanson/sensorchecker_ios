//  User.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import GRDBCombine
import Combine
import Then

struct User: Hashable, Identifiable  {
    var id: Int64
    var firstname: String
    var lastname: String
}

// MARK: - Persistence

extension User: Codable, FetchableRecord, MutablePersistableRecord {
    private enum Columns{
        static let id = Column(CodingKeys.id)
        static let firstname = Column(CodingKeys.firstname)
        static let lastname = Column(CodingKeys.lastname)
    }
    
    // Update a user id after it has been inserted in the database.
    mutating func didInsert(with rowID: Int64, for column: String?) {
        id = rowID
    }
}

// MARK: - Database Access

/// #User DAO
/// Since the user here is quite a different table, we can't conform it to DAOCache. hence why we don't need the `Comparable` conformance.
extension User {
    /// Retrieves the user from the database.
    /// It should be only one user at a time.
    ///
    /// - Returns:The user in the database, or nil if error or it is not present.
    static func retrieveUser() -> User? {
        do {
            return try database.read {db in
                return try User.fetchOne(db, sql: "SELECT * from user limit 1")
            }
        } catch {
            return nil
        }
    }
    
    /// Retrieves the user as an observable object via DatabasePublishers.
    ///
    /// - Returns: A Publisher we can subscribe to, to receive updates.
    static func retrieveObservableUser() -> DatabasePublishers.Value<User?> {
        return ValueObservation
            .tracking{db in
                let users = try User.fetchAll(db, sql: "SELECT * from user limit 1")
                guard users.count > 0 else {return nil}
                return users[0]
        }.publisher(in: database)
    }
    
    /// inserts a new user in the database, and deletes the previous.
    ///
    /// - Parameter user: The user to insert
    /// - Returns: the inserted user (the id can have changed)
    static func insertUser(user: User) -> User? {
        var userToInsert = user
        do {
            try database.write {db in
                try User.deleteAll(db)
                try userToInsert.insert(db)
            }
            return userToInsert
        } catch {
            return nil
        }
    }
}
