//
//  AddDatabase.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 12/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import GRDB
import Then
import UIKit

struct AppDatabase {
    static func openDatabase(atPath path: String) throws -> DatabasePool {
        let dbPool = try DatabasePool(path: path)
        
        // Define the database schema
        try migrator.migrate(dbPool)
        
        return dbPool
    }
    
    static var migrator: DatabaseMigrator {
        var migrator = DatabaseMigrator()
        #if DEBUG
        // Speed up development by nuking the database when migrations change
        migrator.eraseDatabaseOnSchemaChange = true
        #endif
        migrator.registerMigration("databaseSchema") { db in
            try db.create(table: "user") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("firstname", .text).notNull(onConflict: .abort)
                t.column("lastname", .text).notNull(onConflict: .abort)
            }
            try db.create(table: "tour") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("date", .datetime).notNull()
            }
            try db.create(table: "measurementType") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("measurement", .text).notNull()
                t.column("unit", .text).notNull()
            }
            try db.create(table: "action") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("actionLabel", .text).notNull()
            }
            try db.create(table: "sensorState") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("stateCode", .text).notNull()
                t.column("stateDesc", .text).notNull()
            }
            try db.create(table: "sensor") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("idMeasurementType", .integer)
                    .notNull()
                    .indexed()
                    .references("measurementType", column: "id", onDelete: .none, onUpdate: .none, deferred: true)
                t.column("reference", .text).notNull()
                t.column("description", .text).notNull()
                t.column("supplier", .text).notNull()
                t.column("idState", .integer)
                .notNull()
                .indexed()
                    .references("sensorState", column: "id", onDelete: .none, onUpdate: .none, deferred: true)
                t.column("isModified", .boolean).notNull()
            }
            try db.create(table: "tourSensor") {t in
                t.column("id", .integer).primaryKey(onConflict: .replace, autoincrement: false)
                t.column("idTour", .integer)
                    .notNull()
                    .indexed()
                    .references("tour", column: "id", onDelete: .cascade, onUpdate: .cascade, deferred: true)
                t.column("idSensor", .integer)
                    .notNull().indexed()
                    .references("sensor", column: "id", onDelete: .none, onUpdate: .none, deferred: true)
                t.column("sensorOrder", .integer)
                    .notNull()
            }
            try db.create(table: "intervention") {t in
                t.autoIncrementedPrimaryKey("id", onConflict: .replace)
                t.column("idAction", .integer)
                    .notNull().indexed()
                    .references("action", column: "id", onDelete: .none, onUpdate: .none, deferred: true)
                t.column("idTourSensor", .integer)
                    .notNull().indexed()
                    .references("tourSensor", column: "id", onDelete: .cascade, onUpdate: .cascade, deferred: true)
                t.column("comments", .text)
                t.column("idNewSensor", .integer)
            }
        }
        
        return migrator
    }
    
    
    /// Deletes the cache.
    /// - Throws: SQLErrors if it didn't happen nicely
    static func deleteEverthingInDatabase() throws {
        try database.writeInTransaction {db in
            try User.deleteAll(db)
            try Tour.deleteAll(db)
            try Intervention.deleteAll(db)
            try TourSensor.deleteAll(db)
            try Sensor.deleteAll(db)
            try MeasurementType.deleteAll(db)
            try Action.deleteAll(db)
            return .commit
        }
    }
    
    /// Gets everything from the server. Runs only if the app is in foreground.
    /// - Throws: NetworkErrors if something happened. It is rerouted as a No Internet error.
    static func synchronizeWithServer() throws {
        if UIApplication.shared.applicationState == .active {
            ActionDAL.synchronizeActions()
                .bridgeError(to: NetworkError(kind: .noInternet, error: nil))
                .then(MeasurementTypesDAL.synchronizeMeasurements())
                .then(SensorStateDAL.synchronizeStates())
                .then(TourDAL.synchronizeTours())
                .then(SensorDAL.synchronizeSensors())
        } else {
            debugPrint("It's not active lol")
        }
    }
}
