//
//  DataTransferObjects.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation

/// The login response, as it is returned by the API.
struct LoginResponse: Codable {
    let isAuthenticated: Bool
    let userId: Int
    let firstname: String
    let lastname: String
    let authToken: String
    let refreshToken: String
}

struct NetworkActionsContainer: Codable {
    var actions: [NetworkAction]
}

struct NetworkAction: Codable {
    var actionId: Int
    var label: String
}

struct NetworkMeasurementsContainer: Codable {
    var measurements: [NetworkMeasurement]
}

struct NetworkMeasurement: Codable {
    var typeId: Int
    var measurement: String
    var unit: String
}

struct NetworkSensorsContainer: Codable {
    var sensors: [NetworkSensor]
}

struct NetworkSensor: Codable {
    var sensorId: Int
    var measurementId: Int
    var reference: String
    var description: String
    var supplier: String
    var model: String
    var stateId: Int
    var positionId: Int
}

struct NetworkTourSensorsContainer: Codable {
    var tourSensors: [NetworkTourSensor]
}

struct NetworkTourSensor: Codable {
    var tourSensorId: Int
    var tourId: Int
    var sensorId: Int
    var order: Int
}

struct NetworkStatesContainer: Codable {
    var states: [NetworkState]
}

struct NetworkState: Codable {
    var stateId: Int
    var stateCode: String
    var label: String
}

struct NetworkToursContainer: Codable {
    var tours: [NetworkTour]
}

struct NetworkTour: Codable {
    var tourId: Int
    var date: Date
    var userId: Int
    var isModel: Bool?
    var modelName: String?
}

struct NetworkIntervention: Codable {
    var tourSensorId: Int64
    var comments: String
    var newSensorId: Int64?
}

struct RefreshTokenResponse: Codable {
    var token: String
}
