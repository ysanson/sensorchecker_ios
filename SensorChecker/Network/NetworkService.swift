//
//  NetworkService.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Alamofire
import Then

struct NetworkService: Networkable {
    // The base URL for the networking
    private let BASE_URL: String = "https://sensor-check-mock.herokuapp.com/"
    // The JSON decoder, to parse the date at ISO 8601 format
    private let decoder: JSONDecoder = {
        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .custom { decoder in
            let dateStr = try decoder.singleValueContainer().decode(String.self)
            let formatter = ISO8601DateFormatter()
            return formatter.date(from: dateStr) ?? Date.init()
        }
        return jsonDecoder
    }()
    // The networking session
    private var session: Session!
    
    // Tests if the phone is connected to the internet.
    private var isConnectedToInternet: Bool {
        return NetworkReachabilityManager()?.isReachable ?? false
    }
    
    //The different interceptors to use during the session.
    let interceptors = Interceptor(
               adapters: [],
               retriers: [RetryPolicy(retryLimit: 1, retryableHTTPStatusCodes: [500], retryableURLErrorCodes: [.timedOut, .backgroundSessionWasDisconnected, .notConnectedToInternet])],
               interceptors: [AuthInterceptor(), Code53RequestRetrier()])
    
    
    // At launch, instanciates the networking session.
    init() {
        session = Session(interceptor: interceptors)
    }
    
    /// Performs the request vie alamofire. Provides an abstraction to the boilerplate code.
    ///
    /// #Parameters
    /// - endpoint: The endpoint to access, after the base url.
    /// - method: the HTTP method to perform (get, post, put, patch...)
    /// - params: the parameters associated with the request, be it for a query or for a JSON. defaults to null
    /// - encoding: the encoding for these params. Defaults to URLEncoding, but can be JSONEncoding.
    ///
    /// - Returns: Returns a promise from the _Then_ package, containing a **Data** object. This can later be used to instanciate data structures.
    private func performRequest(withEndpoint endpoint: String, withMethod method: HTTPMethod, withParams params: Parameters? = nil, withEncoding encoding: ParameterEncoding = URLEncoding.default)
        -> Promise<Data> {
        return Promise{resolve, reject in
            guard self.isConnectedToInternet else {
                reject(NetworkError(kind: .noInternet, error: nil))
                return
            }
            self.session.request("\(self.BASE_URL)\(endpoint)", method: method, parameters: params, encoding: encoding).validate().responseData {response in
                switch response.result {
                case .failure(let error):
                    debugPrint("Error: \(error)")
                    reject(NetworkError(kind: .networkFailure, error: error))

                case .success(let data):
                    resolve(data)
                }
            }
        }
    }
    
    /// Calls the route to authenticate the user.
    ///
    /// - Parameter username: The given username
    /// - Parameter password: the given password (in plain text)
    
    /// - Returns: A promise containing a  LoginResponse
    func authenticateUser(withUsername username: String, withPassword password: String) -> Promise<LoginResponse> {
        return Promise { resolve, reject in
            let 🛠️: Parameters = ["username": username, "password": password]
            self.performRequest(withEndpoint: "login", withMethod: .post, withParams: 🛠️, withEncoding: JSONEncoding.default)
                .then {data in
                    do {
                        let loginResponse =  try self.decoder.decode(LoginResponse.self, from: data)
                        resolve(loginResponse)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    /// Gets the actions from the network.
    ///
    /// - Returns: A Promise containing an array of NetworkActions.
    func getActions() -> Promise<[NetworkAction]> {
        return Promise { resolve, reject in
            self.performRequest(withEndpoint: "action", withMethod: .get)
                .then {data in
                    do {
                        let 🔧 = try self.decoder.decode(NetworkActionsContainer.self, from: data)
                        resolve(🔧.actions)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    /// Gets the different measurement types from the network.
    ///
    /// - Returns: A Promise containing an array of NetworkMeasurements.
    func getMeasurements() -> Promise<[NetworkMeasurement]> {
        return Promise { resolve, reject in
            self.performRequest(withEndpoint: "measurement", withMethod: .get)
                .then {data in
                    do {
                        let 📏 = try self.decoder.decode(NetworkMeasurementsContainer.self, from: data)
                        resolve(📏.measurements)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    func getSensors() -> Promise<[NetworkSensor]> {
        return Promise { resolve, reject in
            self.performRequest(withEndpoint: "sensor", withMethod: .get)
                .then {data in
                    do {
                        let 🌡️ = try self.decoder.decode(NetworkSensorsContainer.self, from: data)
                        resolve(🌡️.sensors)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    func getStates() -> Promise<[NetworkState]> {
        return Promise { resolve, reject in
            self.performRequest(withEndpoint: "state", withMethod: .get)
                .then {data in
                    do {
                        let 💔 = try self.decoder.decode(NetworkStatesContainer.self, from: data)
                        resolve(💔.states)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    func getTourSensors() -> Promise<[NetworkTourSensor]> {
        return Promise { resolve, reject in
            self.performRequest(withEndpoint: "tourSensor", withMethod: .get)
                .then {data in
                    do {
                        let 🌡️ = try self.decoder.decode(NetworkTourSensorsContainer.self, from: data)
                        resolve(🌡️.tourSensors)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    func getTours(withUserId userId: Int) -> Promise<[NetworkTour]> {
        return Promise { resolve, reject in
            self.performRequest(withEndpoint: "tour", withMethod: .get, withParams: ["userId": userId])
                .then {data in
                    do {
                        let 🏃 = try self.decoder.decode(NetworkToursContainer.self, from: data)
                        resolve(🏃.tours)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    /// Posts the new interventions to the webservice. Sends a POST request.
    /// Contains every interventions to insert, as it is more network efficient to just send one intervention only.
    /// The array is encoded as a JSON String before being sent.
    ///
    /// - Returns:A promise containing a boolean. Although this value can be discarded.
    func postInterventions(interventions: [NetworkIntervention]) -> Promise<Bool> {
        return Promise {resolve, reject in
            do {
                let data = try JSONEncoder().encode(interventions)
                let params = ["interventions": String(data: data, encoding: .utf8)!]
                self.performRequest(withEndpoint: "intervention", withMethod: .post, withParams: params, withEncoding: JSONEncoding.default)
                    .then {data in
                            resolve(true)
                    }.onError {error in reject(error)}
            } catch let error {
                debugPrint(error)
                reject(NetworkError(kind: .badJSON, error: error))
            }
        }
    }
    
    /// Updates a sensor in the webservice, by sending a PATCH request on the /sensor route.
    /// Only sends the sensor ID, and the state, as it's the only thing changed by this application.
    ///
    /// - Parameter id: the sensor ID to send
    /// - Parameter state: the sensor state ID to update
    ///
    /// - Returns:A Promise, containing a boolean for whether it was successful or not. But it's only true, and errors are sent via the reject method.
    func updateSensor(byId id: Int, withState state: Int) -> Promise<Bool> {
        return Promise {resolve, reject in
            let parameters = ["sensorId": id, "stateId": state]
            self.performRequest(withEndpoint: "sensor/\(id)", withMethod: .patch, withParams: parameters, withEncoding: JSONEncoding.default)
                .then {data in
                    resolve(true)
            }.onError {error in reject(error)}
        }
    }
    
    /// Tries to refresh the token in the database. In general, an access token is only valid for a limited amount of time.
    /// This is used by the auth interceptor to automatically perform a refresh operation whenever the token expired.
    ///
    /// - Parameter token: The refresh token, stored in the application.
    ///
    /// - Returns: A Promise containing the renewed access token.
    func refreshToken(withRefreshToken token: String) -> Promise<String> {
        return Promise { resolve, reject in
            let param = ["refreshToken": token]
            self.performRequest(withEndpoint: "token", withMethod: .post, withParams: param, withEncoding: JSONEncoding.default)
                .then {data in
                    do {
                        let 🔑 = try self.decoder.decode(RefreshTokenResponse.self, from: data)
                        resolve(🔑.token)
                    } catch let error {
                        debugPrint("Error decoding the JSON")
                        reject(NetworkError(kind: .badJSON, error: error))
                    }
            }.onError {error in reject(error)}
        }
    }
    
    /// Cancels every task that may use the network in background.
    func cancelSession() {
        session.session.invalidateAndCancel()
    }
    
    /// Resumes the session after invalidating.
    mutating func resumeSession() {
        session = Session(interceptor: interceptors)
    }
}
