//
//  AuthInterceptor.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Alamofire

final class AuthInterceptor: RequestInterceptor {
    
    //Catches the request and adds a token where necessary
    //If it's met with a 401, goes to the retry function.
    func adapt(_ urlRequest: URLRequest, for session: Session, completion: @escaping (Result<URLRequest, Error>) -> Void) {
        
        if (urlRequest.url?.absoluteString.contains("login") == true) || (urlRequest.url?.absoluteString.contains("token") == true) {
            /// If the request does not require authentication, we can directly return it as unmodified.
            return completion(.success(urlRequest))
        }
        
        var urlRequest = urlRequest
        
        guard let accessToken: String = TokensAccessLayer.getAccessToken() else { return completion(.failure(NetworkError(kind: .noToken, error: nil))) }
        
        /// Set the Authorization header value using the access token.
        urlRequest.setValue("Bearer \(accessToken)", forHTTPHeaderField: "Authorization")
        
        completion(.success(urlRequest))
    }
    
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        guard let response = request.task?.response as? HTTPURLResponse, response.statusCode == 401 else {
            /// The request did not fail due to a 401 Unauthorized response.
            /// Return the original error and don't retry the request.
            debugPrint("Error due to \(error)")
            return completion(.doNotRetryWithError(error))
        }
        guard let refreshToken: String = TokensAccessLayer.getRefreshToken() else {return completion(.doNotRetry)}
    
        networkService.refreshToken(withRefreshToken: refreshToken)
            .then {token in
                TokensAccessLayer.setAccessToken(accessToken: token)
                completion(.retry)
        }.onError {error in
            completion(.doNotRetryWithError(error))
        }
    }
}
