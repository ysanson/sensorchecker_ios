//
//  NetworkProtocol.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then

/// #Networkable protocol
///
/// Provides methods to access the distant API.
/// This protocol is used to abstract the real implementation.
protocol Networkable {
    /// Calls the route to authenticate the user.
    ///
    /// - Parameter username: the given username
    /// - Parameter password: the given password (in plain text)
    
    /// - Returns: A promise containing a  LoginResponse
    func authenticateUser(withUsername username: String, withPassword password: String) -> Promise<LoginResponse>
    
    /// Gets the actions from the network.
    ///
    /// - Returns: A Promise containing an array of NetworkActions.
    func getActions() -> Promise<[NetworkAction]>
    
    /// Gets the different measurement types from the network.
    ///
    /// - Returns: A Promise containing an array of NetworkMeasurements.
    func getMeasurements() -> Promise<[NetworkMeasurement]>
    
    func getSensors() -> Promise<[NetworkSensor]>
    
    func getStates() -> Promise<[NetworkState]>
    
    func getTourSensors() -> Promise<[NetworkTourSensor]>
    
    func getTours(withUserId userId: Int) -> Promise<[NetworkTour]>
    
    func postInterventions(interventions: [NetworkIntervention]) -> Promise<Bool>
    
    /// Updates a sensor in the webservice, by sending a PATCH request on the /sensor route.
    /// Only sends the sensor ID, and the state, as it's the only thing changed by this application.
    ///
    /// - Parameter id: the sensor ID to send
    /// - Parameter state: the sensor state ID to update
    ///
    /// - Returns:A Promise, containing a boolean for whether it was successful or not. But it's only true, and errors are sent via the reject method.
    func updateSensor(byId id: Int, withState state: Int) -> Promise<Bool>
    
    /// Tries to refresh the token in the database. In general, an access token is only valid for a limited amount of time.
   /// This is used by the auth interceptor to automatically perform a refresh operation whenever the token expired.
   ///
   /// - Parameter token: The refresh token, stored in the application.
   ///
   /// - Returns: A Promise containing the renewed access token.
    func refreshToken(withRefreshToken token: String) -> Promise<String>
    
    /// Cancels every task that may use the network in background.
    func cancelSession()
    
    /// Resumes the session after invalidating.
    mutating func resumeSession()
}


/// Represents the different cases of network errors.
/// Wraps in the original error, if needed.
struct NetworkError: Error {
    enum ErrorKind {
        case badResponse
        case badJSON
        case networkFailure
        case noToken
        case noInternet
        case Unauthorized
    }
    
    let kind: ErrorKind
    let error: Error?
}
