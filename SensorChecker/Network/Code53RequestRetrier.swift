//
//  Code53RequestRetrier.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 18/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Alamofire

struct Code53RequestRetrier: RequestInterceptor {
    func retry(_ request: Request, for session: Session, dueTo error: Error, completion: @escaping (RetryResult) -> Void) {
        debugPrint("Retrying with error \(error)")
        let nsError = error as NSError
        guard request.retryCount < 3 else { completion(.doNotRetry); return }
        if nsError.domain == "NSPOSIXErrorDomain" && nsError.code == 53 {
            print("🚧retrying, Software caused connection abort")
            completion(.doNotRetry)
            return
        }
        if nsError.domain == "NSURLErrorDomain" && nsError.code == -1005 {
            print("🚧retrying, network connection was lost")
            completion(.doNotRetry)
            return
        }
        completion(.doNotRetry)
    }
    
}
