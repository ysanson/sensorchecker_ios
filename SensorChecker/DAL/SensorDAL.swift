//
//  SensorDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then

struct SensorDAL {
    
    /// Removes our modified from the sensors we receive.
    ///
    /// #3 cases:
    ///
    /// - `setModifiedSensors` returns nil -> we return the networks sensors untouched.
    /// - We have no modified sensors. We return the network untouched.
    /// - We have modified sensors: We filter out those sensors, and return the rest.
    ///
    /// - Parameter networkSensors: The received sensors from the webservice.
    /// - Returns: The sensors to insert.
    private static func removeModifiedSensors(networkSensors: [Sensor]) -> [Sensor] {
        guard let modifiedSensors: [Sensor] = Sensor.getModifiedSensors() else {
            return networkSensors
        }
        
        if modifiedSensors.count == 0 {
            return networkSensors
        } else {
           let modifIDs: [Int64] = modifiedSensors.map { $0.id }
           return networkSensors.filter { !modifIDs.contains($0.id) }
        }
    }
    
    /// Syncs the existing sensors with what is received from the webservice.
    ///
    /// #Attention
    /// This does not touch the modified sensors. It only updates/deletes the unmodiied sensors.
    static func synchronizeSensors() -> Promise<Bool> {
        return Promise {resolve, reject in
            async {
                let netSensors = try await(networkService.getSensors())
                do {
                    let sensors = netSensors.toDB().sorted { $0.id < $1.id }
                    guard sensors.count > 0, let unmodifiedDBSensors: [Sensor] = Sensor.getUnmodifiedSensors() else {
                        resolve(false)
                        return
                    }
                    // We remove the sensors that we received but are modified locally
                    let unmodifiedSensorsToInsert = removeModifiedSensors(networkSensors: sensors)
                    
                    try Sensor.synchronizeTableWithServer(withNewData: unmodifiedSensorsToInsert, extistingData: unmodifiedDBSensors)
                    
                    let netTS = try await(networkService.getTourSensors())
                    let tourSensors = netTS.toDB().sorted { $0.id < $1.id }
                    guard tourSensors.count > 0, let DBTS: [TourSensor] = TourSensor.retrieveAll() else {
                        resolve(false)
                        return
                    }
                    try TourSensor.synchronizeTableWithServer(withNewData: tourSensors, extistingData: DBTS)
                    resolve(true)
                }
                catch let error {
                    debugPrint(error)
                    reject(error)
                }
            }.onError {error in
                debugPrint(error)
                reject(error)
            }
        }
    }
    
    /// Sends every sensor that was marked as _modified_.
    ///
    /// - Returns: A promise containing a boolean, true if eveything went well, false if there are no modified sensors.
    static func sendUpdatedSensors() -> Promise<Bool> {
        return Promise {resolve, reject in
            guard let updatedSensors = Sensor.getModifiedSensors() else {
                resolve(false)
                return
            }
            if updatedSensors.count == 0 {
                resolve(false)
                return
            }
            debugPrint(updatedSensors)
            updatedSensors.forEach { (sensor) in
                let res = try! await(networkService.updateSensor(byId: Int(sensor.id), withState: Int(sensor.idState)))
                debugPrint(res)
            }
            debugPrint("We passed there")
            resolve(true)
        }
    }
}


extension Array where Element == NetworkSensor {
    func toDB() -> [Sensor] {
        return map {sensor in Sensor(id: Int64(sensor.sensorId), idMeasurementType: Int64(sensor.measurementId), reference: sensor.reference, description: sensor.description, supplier: sensor.supplier, idState: Int64(sensor.stateId), isModified: false)}
    }
}

extension Array where Element == NetworkTourSensor {
    func toDB() -> [TourSensor] {
        return map {ts in TourSensor(id: Int64(ts.tourSensorId), idTour: Int64(ts.tourId), idSensor: Int64(ts.sensorId), sensorOrder: Int8(ts.order))}
    }
}
