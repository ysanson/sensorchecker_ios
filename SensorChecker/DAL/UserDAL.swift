//
//  UserDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then
import Combine

struct UserDAL {
    
    var user = User.retrieveObservableUser()
    
    /// Tries to authenticate the user, given the credentials it has given.
    /// - Parameters:
    ///    - username: the given username
    ///    - password: the given password (in plain text)
    /// - Returns: A promise of completion, true if the user was authenticated, false otherwise.
    static func authenticateUser(withUsername username: String, withPassword password: String) -> Promise<Bool>{
        return Promise {resolve, reject in
            networkService.authenticateUser(withUsername: username, withPassword: password)
                .then { loginResponse in
                    if loginResponse.isAuthenticated {
                        let user = User(id: Int64(loginResponse.userId), firstname: loginResponse.firstname, lastname: loginResponse.lastname)
                        _ = User.insertUser(user: user)
                        TokensAccessLayer.setTokens(accessToken: loginResponse.authToken, refreshToken: loginResponse.refreshToken)
                        resolve(true)
                    } else {
                        resolve(false)
                    }
                }.onError {error in reject(error)}
        }
    }
    
    /// Deletes everything in database, and removes the tokens.
    static func logout() {
        do {
            try AppDatabase.deleteEverthingInDatabase()
            TokensAccessLayer.deleteTokens()
        } catch let error {
            debugPrint(error)
        }
    }
}
