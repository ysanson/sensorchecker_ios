//
//  TourDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then

struct TourDAL {
    
    /// Retrieves everything from the server, and returns a promise of completion to guarantee the synchronicity between transactions.
    static func synchronizeTours() -> Promise<Bool> {
        return Promise {resolve, reject in
            async {
                guard let user = User.retrieveUser() else {return}
                let networkTours = try ..networkService.getTours(withUserId: Int(user.id))
                do {
                    let tours = networkTours.toDB().sorted {$0.id < $1.id}
                    guard tours.count > 0, let DBTours: [Tour] = Tour.retrieveAll() else {
                        resolve(false)
                        return
                    }
                    try Tour.synchronizeTableWithServer(withNewData: tours, extistingData: DBTours)
                    resolve(true)
                } catch let error {
                    debugPrint(error)
                    reject(error)
                }
            }.onError {error in
                debugPrint(error)
                reject(error)
            }
        }
    }
}


extension Array where Element == NetworkTour {
    func toDB() -> [Tour] {
        return map {tour in Tour(id: Int64(tour.tourId), date: tour.date)}
    }
}
