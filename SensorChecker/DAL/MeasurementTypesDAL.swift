//
//  MeasurementTypesDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Combine
import Then

struct MeasurementTypesDAL {
    
    /// Retrieves everything from the server, and returns a promise of completion to guarantee the synchronicity between transactions.
    static func synchronizeMeasurements() -> Promise<Bool> {
        return Promise {resolve, reject in
            async {
                let networkMeasurements = try ..networkService.getMeasurements()
                do {
                    let measurements = networkMeasurements.toDB().sorted { $0.id < $1.id }
                    guard measurements.count > 0, let DBMeasurements: [MeasurementType] = MeasurementType.retrieveAll() else {
                        resolve(false)
                        return
                    }
                    try MeasurementType.synchronizeTableWithServer(withNewData: measurements, extistingData: DBMeasurements)
                    resolve(true)
                } catch let error {
                    debugPrint("Ah ben merde, tiens : \(error)")
                    reject(error)
                }
            }.onError{error in
                debugPrint("Error \(error)")
                reject(error)
            }
        }
    }
}


extension Array where Element == NetworkMeasurement {
    func toDB() -> [MeasurementType] {
        return map {element in MeasurementType(id: Int64(element.typeId), measurement: element.measurement, unit: element.unit)}
    }
}
