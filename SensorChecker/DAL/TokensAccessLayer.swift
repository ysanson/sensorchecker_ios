//
//  TokensAccessLayer.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 15/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import KeychainAccess

/// Provides an abstraction to the token storage in the keychain.
struct TokensAccessLayer {
    private static let keychain = Keychain(service: "com.viveris.sensorChecker")
    
    /// Sets the tokens in the keychain.
    ///
    /// - Parameter accessToken: the access token to set, in JWT format.
    /// - Parameter refreshToken: The refresh token to set (dependant on the app)
    static func setTokens(accessToken: String, refreshToken: String) {
        keychain["accessToken"] = accessToken
        keychain["refreshToken"] = refreshToken
    }
    
    /// Returns the access token stored in the keychain.
    /// - Returns: The access token, or nil if not found.
    static func getAccessToken() -> String? {
        return try? keychain.getString("accessToken")
    }
    
    /// Returns the refresh token stored in the keychain.
    /// - Returns: The refresh token, or nil if not found.
    static func getRefreshToken() -> String? {
        return try? keychain.getString("refreshToken")
    }
    
    /// Sets the access token independantly from the refresh one.
    /// - Parameter accessToken: the access token, as JWT
    static func setAccessToken(accessToken: String) {
        keychain["accessToken"] = accessToken
    }
    
    /// Sets the refresh token independently from the access token.
    /// - Parameter refreshToken: the refresh token
    static func setRefreshToken(refreshToken: String) {
        keychain["refreshToken"] = refreshToken
    }
    
    /// Deletes the tokens in the keychain.
    static func deleteTokens() {
        keychain["accessToken"] = nil
        keychain["refreshToken"] = nil
    }
}
