//
//  ActionDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then
import Combine
import GRDB
import SortedDifference

struct ActionDAL {
    
    /// Retrieves everything from the server, and returns a promise of completion to guarantee the synchronicity between transactions.
    static func synchronizeActions()-> Promise<Bool> {
        return Promise {resolve, reject in
            async {
                let networkActions = try ..networkService.getActions()
                do {
                    let actions = networkActions.toDB().sorted {$0.id < $1.id}
                    guard actions.count > 0, let DBActions: [Action] = Action.retrieveAll() else {
                        resolve(false)
                        return
                    }
                    try Action.synchronizeTableWithServer(withNewData: actions, extistingData: DBActions)
                    resolve(true)
                } catch let error {
                    debugPrint(error)
                    reject(error)
                }
            }.onError {error in
                debugPrint(error)
                reject(error)
            }
        }
    }
}

///Maps the network actions to regular actions used by the app.
extension Array where Element == NetworkAction {
    func toDB() -> [Action] {
        return map {element in Action(id: Int64(element.actionId), actionLabel: element.label)}
    }
}
