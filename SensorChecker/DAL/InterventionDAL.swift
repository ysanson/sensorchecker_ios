//
//  InterventionDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 22/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then

struct InterventionDAL {
    
    /// Sends the interventions to the distant werbservice.
    ///
    /// - Returns: A promise of completion
    static func sendAllInterventions() -> Promise<Bool> {
        return Promise {resolve, reject in
            guard let interventions = Intervention.retrieveAll() else {
                resolve(false)
                return
            }
            if interventions.count == 0 {
                resolve(false)
                return
            }
            let networkInterventions = interventions.toNetwork()
            networkService.postInterventions(interventions: networkInterventions)
                .then {res in resolve(res)}
                .onError {error in reject(error)}
        }
    }
}

/// Maps an array of interventions to an array of network interventions.
extension Array where Element == Intervention {
    func toNetwork() -> [NetworkIntervention] {
        return map { int in
            NetworkIntervention(tourSensorId: int.idTourSensor, comments: int.comments ?? "", newSensorId: int.idNewSensor)
        }
    }
}
