//
//  SensorStateDAL.swift
//  SensorChecker
//
//  Created by Yvan Sanson on 16/06/2020.
//  Copyright © 2020 Yvan Sanson. All rights reserved.
//

import Foundation
import Then

struct SensorStateDAL {
    
    /// Retrieves everything from the server, and returns a promise of completion to guarantee the synchronicity between transactions.
    static func synchronizeStates() -> Promise<Bool> {
        return Promise {resolve, reject in
            async {
                let networkStates = try ..networkService.getStates()
                do {
                    let states = networkStates.toDB().sorted { $0.id < $1.id }
                    guard states.count > 0, let DBStates: [SensorState] = SensorState.retrieveAll() else {
                        resolve(false)
                        return
                    }
                    try SensorState.synchronizeTableWithServer(withNewData: states, extistingData: DBStates)
                    resolve(true)
                } catch let error {
                    debugPrint("Ah ben merde, tiens : \(error)")
                    reject(error)
                }
            }.onError{error in
                debugPrint("Error \(error)")
                reject(error)
            }
        }
    }
}

extension Array where Element == NetworkState {
    func toDB() -> [SensorState] {
        return map {el in SensorState(id: Int64(el.stateId), stateCode: el.stateCode, stateDesc: el.label)}
    }
}
